In this R file you can have a look at a differential genes expressions analysis concerning bladder cancer data from bladderbatch package. We used limma library to do it. It allows to reduce the quantity of genes to study and focus on the most differentially expressed ones. Furthermore, if you want to create classifiers, perform predictions, you can use this method before. It will allow to reduce time and cost for your analysis. In addition to the R file, you can have a look at the HTML output created with Rmarkdown.
Have fun !

Author : Marion Estoup

Mail : marion_110@hotmail.fr

Date : June 2023