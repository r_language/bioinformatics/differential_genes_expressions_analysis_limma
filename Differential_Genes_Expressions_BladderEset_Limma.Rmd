---
title: "Differential Genes Expressions Analysis with Limma library"
output: html_document
date: "2023-06-26"
author : Marion Estoup
adress : marion_110@hotmail.fr
---


# Main links : https://www.biostat.jhsph.edu/~jleek/papers/sva-appnote-supp.pdf

```{r message=FALSE, warning=FALSE, results='hide'}

# Load libraries
options(width = 10)
library(sva)
library(bladderbatch)
library(limma)
library(pamr)
library(RColorBrewer)

# Load data
data(bladderdata)
```

For the bladder cancer study, the variable of interest is cancer status. To begin we'll assume no adjustment variables. The bladder data are stored in an expression set - a Bioconductor object used for storing gene expression data. The variables are stored in the phenotype data slot and can be obtained as follows:

```{r echo=TRUE}
# Phenotype data
pheno = pData(bladderEset)
```

# The expression data can be obtained from the expression slot of the expression set.
```{r echo=TRUE}
# Expression data
edata = exprs(bladderEset)
```

# Applying the fsva function to remove batch effects for prediction

The surrogate variable analysis functions have been developed for population-level analyses such as differential expression analysis in microarrays. In some cases, the goal of an analysis is prediction. In this case, data sets are generally composed a training set and a test set. For each sample in the training set, the outcome/class is known, but latent sources of variability are unknown. For the samples in the test set, neither the outcome/class or the latent sources of variability are known. “Frozen” surrogate variable analysis can be used to remove latent variation in the test dataset. To illustrate these functions, the bladder data can be separated into a training and test set.

```{r echo=TRUE}
# Set seed and create train and test data
set.seed(12354)
trainIndicator = sample(1:57,size=30,replace=F)
testIndicator = (1:57)[-trainIndicator]
trainData = edata[,trainIndicator]
testData = edata[,testIndicator]
trainPheno = pheno[trainIndicator,]
testPheno = pheno[testIndicator,]
```

Using these data sets, the pamr package can be used to train a predictive model on the training data, as well as test that prediction on a test data set.

```{r echo=TRUE}
# Create train and test data with pamr
mydata = list(x=trainData,y=trainPheno$cancer)
mytrain = pamr.train(mydata)
table(pamr.predict(mytrain,testData,threshold=2),testPheno$cancer)
```

```{r echo=TRUE}
# Next, the sva function can be used to calculate surrogate variables for the training set.
trainMod = model.matrix(~cancer,data=trainPheno)
trainMod0 = model.matrix(~1,data=trainPheno)
trainSv = sva(trainData,trainMod,trainMod0)
```

The fsva function can be used to adjust both the training data and the test data. The training data is adjusted using the calculated surrogate variables. The testing data is adjusted using the “frozen” surrogate variable algorithm (to be submitted). The output of the fsva function is an adjusted training set and an adjusted test set. These can be used to train and test a second, more accurate, prediction function.

```{r echo=TRUE}
# Create train data with pamr thanks to fsva function (more accurate)
fsvaobj = fsva(trainData,trainMod,trainSv,testData)
mydataSv = list(x=fsvaobj$db,y=trainPheno$cancer)
mytrainSv = pamr.train(mydataSv)
table(pamr.predict(mytrainSv,fsvaobj$new,threshold=1),testPheno$cancer)
```


Furthermore, we can cluster both the corrected and uncorrected training samples. Before fsva is applied, the biopsy and normal samples do not all cluster together (Figure 1 left panel). After fsva adjustment, the clustering of the test set is improved, with only one cancer sample clustering with the normals and biopsies (Figure 1 right panel).


# Load helper functions

```{r echo=TRUE}
# Create function
myplclust <- function(hclust, lab=hclust$labels,
                      lab.col=rep(1,length(hclust$labels)), hang=0.1,...){
  ## modifiction of plclust for plotting hclust objects *in colour*!
  ## Copyright Eva KF Chan 2009
  ## Arguments:
  ## hclust: hclust object
  ## lab: a character vector of labels of the leaves of the tree
  ## lab.col: colour for the labels; NA=default device foreground colour
  ## hang: as in hclust & plclust
  ## Side effect:
  ## A display of hierarchical cluster with coloured leaf labels.
  y <- rep(hclust$height,2)
  x <- as.numeric(hclust$merge)
  y <- y[which(x<0)]
  x <- x[which(x<0)]
  x <- abs(x)
  y <- y[order(x)]
  x <- x[order(x)]
  plot( hclust, labels=FALSE, hang=hang, ... )
  text( x=x, y=y[hclust$order]-(max(hclust$height)*hang),
        labels=lab[hclust$order], col=lab.col[hclust$order],
        srt=90, adj=c(1,0.5), xpd=NA, ... )}

```

```{r echo=TRUE}
# Create function for palette colors
mypar <- function(a=1,b=1,brewer.n=8,brewer.name="Dark2",...){
  par(mar=c(2.5,2.5,1.6,1.1),mgp=c(1.5,.5,0))
  par(mfrow=c(a,b),...)
  palette(brewer.pal(brewer.n,brewer.name))
  }
```


# Make plots

```{r echo=TRUE}
# Before sva
mypar()
tmp = apply(testData,1,var)
keep = which(rank(-tmp) < 2000)
hBefore = hclust(dist(t(testData[keep,])))

# After sva
tmp2 = apply(fsvaobj$new,1,var)
keep2 = which(rank(-tmp2) < 2000)
hAfter = hclust(dist(t(fsvaobj$new[keep2,])))
par(mfrow=c(1,2))
```

```{r echo=TRUE}
# Apply created function on test data created before fsva function and on test data created after using fsva function to see the differences
myplclust(hBefore,lab=testPheno$cancer,
          lab.col=as.numeric(testPheno$cancer),main="Before fsva",xlab="")

myplclust(hAfter,lab=testPheno$cancer,
          lab.col=as.numeric(testPheno$cancer),main="After fsva",xlab="")
      
```

In the figure above, we observe a clustering of the test samples in the bladder cancer study before (left) and after (right) adjustment with the fsva function. After using the fsva function, the normal and biopsy samples cluster together and only one cancer sample is “misclustered”.


# Store top genes
```{r echo=TRUE}
# Store top genes/features to use only those genes and their expression values
topgenes_limma <- data.frame(keep2)
topgenes_limma <- rownames(topgenes_limma)

# Retrieve expression values corresponding to the top genes obtained with limma library and not all genes
topgenes_matrix = edata[topgenes_limma,]

# If it's needed to put samples in rows and genes in columns we can do :
#transposed_edata <- t(edata)
# We keep only the expression values for the genes kept with limma package 
# (around 2000 genes instead of 22000)
#topgenes_limma = transposed_edata[,topgenes_limma]

# Now that we have the most significant features (genes) selected/kept with limma
# We can use this expression set data to create classifiers for predictions, select top-k genes among those top differentially expressed genes selected with limma
# So here, topgenes_limma contains around 2 000 genes, while the original dataset bladderbatch contained around 22 000 genes. It allows to reduce time and cost when creating classifiers, predictions, finding top k genes, etc and focus on the most differentially expressed genes for further analysis
```






